package com.templatereportengine.utils.docx;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import com.templatereportengine.abstracts.AbstractTableColumnFormatter;
import com.templatereportengine.constants.placeholder.table.ColumnFormatterType;
import org.apache.commons.lang.StringUtils;
import org.docx4j.TraversalUtil;
import org.docx4j.XmlUtils;
import org.docx4j.finders.ClassFinder;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.openpackaging.parts.relationships.RelationshipsPart;
import org.docx4j.relationships.Relationship;
import org.docx4j.relationships.Relationships;
import org.docx4j.wml.R;
import org.docx4j.wml.P.Hyperlink;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.Tc;
import org.docx4j.wml.Tr;

import com.templatereportengine.impl.placeholders.TablePlaceHolder;
import com.templatereportengine.interfaces.ICustomColumnFormatter;
import com.templatereportengine.interfaces.ITableColumnFormatter;

import org.docx4j.wml.Text;

public class DocxUtils {
	
	/**
	 * Replace a place holder with a table
	 * @param tablePlaceHolder The place holder that represents the table
	 * @param mainDocPart The MainDocumentPart of the template
	 */
	public static void populateTable(TablePlaceHolder tablePlaceHolder, MainDocumentPart mainDocPart) {				
		try {
			Map<String, ITableColumnFormatter> tableColumnFormatter = tablePlaceHolder.getColumnFormatterMap();
			String tableName = tablePlaceHolder.getIdentifier();
			List<?> dataSet = tablePlaceHolder.getDataSet();
			
			// Get the table and it's rows
			Tbl table = getTemplateTable(mainDocPart, tableName);
			Tr templateRow = getTableTemplateRow(table);

			// Go through our list of beans and bind their property values to a
			// new row created from the template row
			Iterator<?> iterator = dataSet.iterator(); 
			while ( iterator.hasNext() ) {
				Object bean = iterator.next();
				addRowToTable(mainDocPart, table, templateRow, bean, tableColumnFormatter);
			}
 
			// Remove the template row
			table.getContent().remove(templateRow);
		} catch (Exception e) {
			System.out.println("DocxUtils.populateTable. Error populating table: " + e);
		}
	}
			
	/**
	 * Replace the value of a Text node element
	 * @param text The org.docx4j.wml.Text object
	 * @param find The String to find
	 * @param replace The String to replace with
	 */
	public static void replaceText(Text text, String find, String replace) {
		replace = "null".equals(replace) ? "" : replace;
		replace = StringUtils.defaultIfEmpty(replace, "");

		String orginialText = text.getValue();
		String modifiedText = orginialText.replace(find, replace);

		text.setValue(modifiedText);
	}
	
	/**
	 * Update the place holder to have a hyperlink
	 * @param mainDocPart MainDocumentPart of the template
	 * @param hyperlinkRElement The R element that holds the hyperlink place holder
	 * @param text The text that should appear hyperlinked in the document
	 * @param url The url (target) of the hyperlink
	 */
	public static void updateHyperlink(MainDocumentPart mainDocPart, R hyperlinkRElement, String text, String url) {
		// Get the hyperlink element (the parent of the R element) 
		Hyperlink hyperlink = (Hyperlink) hyperlinkRElement.getParent();
		String relId = hyperlink.getId();
		
		// Update the relationship element of the hyperlink to have the new URL
	    Relationship rel = mainDocPart.getRelationshipsPart().getRelationshipByID(relId);
	    rel.setTarget(url);
	    
	    List<Object> hyperLinkTextElements = getAllElementsFromObject(hyperlink, Text.class);
	    
	    // The first text item encountered should have the hyperlink text
	    if ( hyperLinkTextElements.size() > 0 ) {
	    	Object temp = XmlUtils.unwrap(hyperLinkTextElements.get(0));
	    	Text textObj = (Text) temp;
	    	textObj.setValue(text);
	    }
	}
	
	/**
	 * Add a row to a table
	 * @param mainDocPart MainDocumentPart of the template
	 * @param table The org.docx4j.wml.Tbl that represents the table
	 * @param templateRow The org.docx4j.wml.Tr that represents the template row ( {TEMPLATE_ROW} )
	 * @param bean The Java bean (POJO) that will be bound to the table
	 * @param tableColumnFormatter Table column formatters
	 * @throws Exception
	 */
	public static void addRowToTable(MainDocumentPart mainDocPart, Tbl table, Tr templateRow, Object bean, Map<String, ITableColumnFormatter> tableColumnFormatter) throws Exception {
		
		// Make a copy of the template row
		Tr newRow = (Tr) XmlUtils.deepCopy(templateRow);
		
		// Get the cell contents and use them as attribute names to get bean values
		List<?> textElements = getAllElementsFromObject(newRow, Text.class);
		for (Object object : textElements) {
			Text cellText = (Text) object;
			
			String cellTextValue = cellText.getValue();
			if ( StringUtils.isNotBlank(cellTextValue) ) {
				
				// Use reflection to get the value of the property on the bean
				String methodName = "get" + StringUtils.capitalize(cellTextValue);
				Method method = bean.getClass().getMethod(methodName);
								
				String replacementValue = String.valueOf(method.invoke(bean));
				replacementValue = "null".equals(replacementValue) ? "" : replacementValue;
				
				// Add the property value to the cell text
				if (replacementValue != null){
					if ( null != tableColumnFormatter ) {
						ITableColumnFormatter formatter = tableColumnFormatter.get(cellTextValue);
						if ( null != formatter ) {
							
							switch ( formatter.getColumnFormatter()) {
							case HYPERLINK:
								// Get hyperlink parent of text field
								updateHyperlinkFromTextElement(mainDocPart, cellText, replacementValue);
								break;
							case CUSTOM:
								replacementValue = formatter.format(bean, replacementValue);
								break;
							default:
								replacementValue = formatter.format(replacementValue);								
								break;
							}
						}
					}
					
					cellText.setValue(replacementValue);				
				}				
			}
		}
 
		table.getContent().add(newRow);
	}
	
	/**
	 * Update a org.docx4j.wml.Text element (that is hyperlinked) to a specified hyperlink
	 * @param mainDocPart The MainDocumentPart of the template
	 * @param textElement The org.docx4j.wml.Text element that contans the place holder
	 * @param urlAndText The url of the hyperlink
	 */
	public static void updateHyperlinkFromTextElement(MainDocumentPart mainDocPart, Text textElement, String urlAndText) {
		R hyperlinkRElement = (R) textElement.getParent();
		
		RelationshipsPart relationshipsPart = mainDocPart.getRelationshipsPart(true);
		org.docx4j.relationships.ObjectFactory factory = new org.docx4j.relationships.ObjectFactory();
		
		Relationship relationship = factory.createRelationship();
		relationship.setType(Namespaces.HYPERLINK);
		relationship.setTarget(urlAndText);
		relationship.setTargetMode("External");
		relationshipsPart.addRelationship(relationship);
		
		Hyperlink hyperlinkElement = (Hyperlink) hyperlinkRElement.getParent();
		hyperlinkElement.setId(relationship.getId());
	}
	
	/**
	 * Find all the tables that have a specified Id
	 * @param mainDocPart MainDocumentPart of the template
	 * @param tableId The id of the table
	 * @return A org.docx4j.wml.Tbl that represents the table
	 * @throws Exception
	 */
	public static Tbl getTemplateTable(MainDocumentPart mainDocPart, String tableId) throws Exception {
		String xpath = "//w:tbl";
		List<Object> tables = mainDocPart.getJAXBNodesViaXPath(xpath, false);
		String removeHeaderProperty = "REMOVE_ROW:true";
		boolean shouldRemoveHeader = false;
		
		Tbl table = null;
		tableId = String.format("TABLE_NAME:%s", tableId);
		
		// Look for the column name in the first cell of the table
		for (Iterator<Object> iterator = tables.iterator(); iterator.hasNext();) {
			Object tbl = iterator.next();
			tbl = (tbl instanceof JAXBElement) ? XmlUtils.unwrap(tbl) : tbl;
			
			List<?> textElements = getAllElementsFromObject(tbl, Text.class);
			
			if ( textElements.size() > 0 ) {
				Text textElement = (Text) textElements.get(0);
				String textElementValue = StringUtils.defaultIfEmpty(textElement.getValue(), "");
				
				table = textElementValue.contains(tableId) ? (Tbl) tbl : null;
				shouldRemoveHeader = textElementValue.contains(removeHeaderProperty);
				
				if ( null != table ) {
					// Remove the TABLE_NAME:blah from the column header
					replaceText(textElement, tableId, "");
					// What's left over is just empty braces which should be removed.
					replaceText(textElement, "{}", "");
					
					if ( shouldRemoveHeader ) {
						List<?> rows = getAllElementsFromObject(tbl, Tr.class);
						table.getContent().remove(rows.get(0));
					}
					
					break;
				}
			}
		}
		return table;
	}
	
	/**
	 * Get the template row {TEMPLATE_ROW} of the table
	 * @param table The table to be searched for the {TEMPLATE_ROW}
	 * @return The org.docx4j.wml.Tr of the table row, if it can't be found NULL is returned
	 * @throws Exception
	 */
	public static Tr getTableTemplateRow(Tbl table) throws Exception {
		final String TEMPLATE_ROW = "{TEMPLATE_ROW}";
		
		// Get all the table rows
		List<?> tableRows = getAllElementsFromObject(table, Tr.class);
		Iterator<?> rowIterator = tableRows.iterator();
		
		Tr row = null;
		
		while ( rowIterator.hasNext() ) {
			Tr currentRow = (Tr) XmlUtils.unwrap(rowIterator.next());
			
			// Get all the cells in the table row
			List<?> tableCells = getAllElementsFromObject(currentRow, Tc.class);
			Iterator<?> cellIterator = tableCells.iterator();
			
			// Check the first cell in each row for the template row marker
			while ( cellIterator.hasNext() ) {
				List<?> cellTextElements = getAllElementsFromObject(cellIterator.next(), Text.class);
				if ( cellTextElements.size() > 0 ) {
					Text cellTextElement = (Text) XmlUtils.unwrap(cellTextElements.get(0));
					String cellTextValue = cellTextElement.getValue();
					
					if ( cellTextValue.contains(TEMPLATE_ROW)) {
						row = currentRow;
						
						// Remove the template row marker from the cell contents
						replaceText(cellTextElement, TEMPLATE_ROW, "");
						break;
					}
				}
			}
			
			
			// We've found what we are looking for so end the loop.
			if ( null != row ) {
				break;
			}
		}
		
		return row;
	}
	
	/**
	 * Get all elements of a specific type in a object
	 * @param obj The object to perform a search for
	 * @param toSearch The elenent to search for within the specified obj
	 * @return A list of objects conforming to the searching
	 */
	public static List<Object> getAllElementsFromObject(Object obj, Class<?> toSearch) {
		ClassFinder finder = new ClassFinder(toSearch);
		new TraversalUtil(obj, finder);
		
		return finder.results;
	}
}

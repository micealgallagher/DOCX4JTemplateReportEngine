package com.templatereportengine.engine;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.docx4j.XmlUtils;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.R;
import org.docx4j.wml.Text;

import com.templatereportengine.constants.placeholder.ValueType;
import com.templatereportengine.impl.placeholders.CustomPlaceHolder;
import com.templatereportengine.impl.placeholders.TablePlaceHolder;
import com.templatereportengine.impl.placeholders.ValueMapPlaceHolder;
import com.templatereportengine.interfaces.ITemplatePlaceHolder;
import com.templatereportengine.interfaces.ITemplateReport;
import com.templatereportengine.utils.docx.DocxUtils;

public  class TemplateReportGenerator {
		
	/**
	 * Generate a DOCX based report from a specified template
	 * @param report The report definition
	 * @param output The file to output the report to
	 * @throws Exception
	 */
	public static void generateReport(ITemplateReport report, File output) throws Exception {
		System.out.println("Beginning the report creation...");
        
        String templateLocation = report.getTemplateLocation();
		File templateFile = new File(templateLocation);
		
		if ( templateFile.exists() ) {
			WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(templateFile);
			MainDocumentPart mainDocPart = wordMLPackage.getMainDocumentPart();
			
			// Start replacing the various place holders
			replaceStringPlaceHolders(mainDocPart, report.getPlaceHoldersByType(ValueType.STRING));
			replaceHyperlinkPlaceHolders(mainDocPart, report.getPlaceHoldersByType(ValueType.HYPERLINK));
			replaceValueMapPlaceHolders(mainDocPart, report.getPlaceHoldersByType(ValueType.VALUE_MAP));
			replaceCustomPlaceHolders(mainDocPart, report.getPlaceHoldersByType(ValueType.CUSTOM));
			replaceTablePlaceHolders(mainDocPart, report.getPlaceHoldersByType(ValueType.TABLE));
			
			wordMLPackage.save(output);
			
			System.out.println("File output to " + output.getAbsolutePath());
		} else {
			throw new FileNotFoundException("The template file cannot be located, please verify it's location " + templateLocation);
		}
	}
	
	/**
	 * Replace all place holders defined as Value Maps
	 * @param mainDocPart MainDocumentPart of the template
	 * @param placeHolders All value map place holders
	 */
	private static void replaceValueMapPlaceHolders(MainDocumentPart mainDocPart, List<ITemplatePlaceHolder> placeHolders){
		System.out.println("    replacing value map place holder");
        
        Iterator<ITemplatePlaceHolder> iterator = placeHolders.iterator();
        while ( iterator.hasNext() ) {
        	ITemplatePlaceHolder placeHolder = iterator.next();
        	
        	// Prepare the XPath that will be applied to the template
        	String xpath = "//w:t[contains(text(),'%s')]";
        	xpath = String.format(xpath, placeHolder.getIdentifier());
        	
        	System.out.println("        XPath used " + xpath);
        	
        	try {
	        	// Get all the nodes conforming to said XPath, they should be org.docx4j.wml.Text
	        	List<Object> list = mainDocPart.getJAXBNodesViaXPath(xpath, false);
	        	        	
	        	// Replace the contents of those Text nodes with the placeholder value
	        	for (Object obj : list) {
	        		Text text = (Text) XmlUtils.unwrap(obj);
	        			        		
	        		String find = placeHolder.getIdentifier();
	        		String key = placeHolder.getValue();
	        		
	        		ValueMapPlaceHolder valueMapPlaceHolder = (ValueMapPlaceHolder) placeHolder;
	        		Map<String, String> valueMap = valueMapPlaceHolder.getValueMap();
	        		
	        		String replace = valueMap.get(key);
	        		replace = StringUtils.defaultIfEmpty(replace, "VALUE NOT MAPPED");
	        		
	        		DocxUtils.replaceText(text, find, replace);
	        	}
        	} catch (Exception e) {
        		System.out.println("An error occurred attempting to update the value map place holders");
        		System.out.println(e);
        	}
        }
	}
	
	/**
	 * Replace all hyperlink place holders
	 * @param mainDocPart MainDocumentPart of the template
	 * @param placeHolders All Hyperlink place holders
	 */
	private static void replaceHyperlinkPlaceHolders(MainDocumentPart mainDocPart, List<ITemplatePlaceHolder> placeHolders){
		System.out.println("    replacing hyperlink place holder");
        
        Iterator<ITemplatePlaceHolder> iterator = placeHolders.iterator();
        while ( iterator.hasNext() ) {
        	ITemplatePlaceHolder placeHolder = iterator.next();
        	
        	// Prepare the XPath that will be applied to the template
        	String xpath = "//w:r[w:t[contains(text(),'%s')]]";
        	xpath = String.format(xpath, placeHolder.getIdentifier());
        	
        	System.out.println("        XPath used " + xpath);
        	
        	try {
        		// Get all the nodes conforming to said XPath, they should be org.docx4j.wml.R
            	List<Object> list = mainDocPart.getJAXBNodesViaXPath(xpath, false);
            	        	
            	// Replace the contents of those Text nodes with the placeholder value
            	for (Object obj : list) {
                    R hyperlinkRElement = (R) obj;	
                    
                    String text = placeHolder.getText();
                    String url = placeHolder.getValue();
            		DocxUtils.updateHyperlink(mainDocPart, hyperlinkRElement, text, url);        		
            	}	
        	} catch (Exception e) {
        		System.out.println("An error occurred attempting to update the hyperlink place holders");
        	}
        }
	}
	
	/**
	 * Replace all string place holders
	 * @param mainDocPart MainDocumentPart of the template
	 * @param placeHolders All string place holders
	 */
	private static void replaceStringPlaceHolders(MainDocumentPart mainDocPart, List<ITemplatePlaceHolder> placeHolders) {
        System.out.println("    replacing string place holder");
        
        Iterator<ITemplatePlaceHolder> iterator = placeHolders.iterator();
        while ( iterator.hasNext() ) {
        	ITemplatePlaceHolder placeHolder = iterator.next();
        	
        	// Prepare the XPath that will be applied to the template
        	String xpath = "//w:t[contains(text(),'%s')]";
        	xpath = String.format(xpath, placeHolder.getIdentifier());
        	
        	System.out.println("        XPath used " + xpath);
        	
        	try {
	        	// Get all the nodes conforming to said XPath, they should be org.docx4j.wml.Text
	        	List<Object> list = mainDocPart.getJAXBNodesViaXPath(xpath, false);
	        	        	
	        	// Replace the contents of those Text nodes with the placeholder value
	        	for (Object obj : list) {
	        		Text text = (Text) XmlUtils.unwrap(obj);
	        		
	        		String find = placeHolder.getIdentifier();
	        		String replace = placeHolder.getValue();
	        		
	        		DocxUtils.replaceText(text, find, replace);
	        	}
        	} catch (Exception e) {
        		System.out.println("An error occurred attempting to update the String place holders");
        	}
        }
	}
	
	/**
	 * Replace all custom place holders
	 * @param mainDocPart MainDocumentPart of the template
	 * @param placeHolders All custom place holders
	 */
	private static void replaceCustomPlaceHolders(MainDocumentPart mainDocPart, List<ITemplatePlaceHolder> placeHolders) {
        System.out.println("    replace the custom place holders");
        
        Iterator<ITemplatePlaceHolder> iterator = placeHolders.iterator();
        while ( iterator.hasNext() ) {
        	ITemplatePlaceHolder placeHolder = iterator.next();
        	
        	// Prepare the XPath that will be applied to the template
        	String xpath = "//w:t[contains(text(),'%s')]";
        	xpath = String.format(xpath, placeHolder.getIdentifier());
        	
        	System.out.println("        XPath used " + xpath);
        	
        	try {
	        	// Get all the nodes conforming to said XPath, it will be up to the callback
        		// method to determine what the object is and how to use it.
	        	List<Object> list = mainDocPart.getJAXBNodesViaXPath(xpath, false);
	        	        	
	        	// Replace the contents of those Text nodes with the placeholder value
	        	for (Object obj : list) {
	        		
	        		CustomPlaceHolder customPlaceHolder = (CustomPlaceHolder) placeHolder;
	        		
	        		Object temp = XmlUtils.unwrap(obj);
	        		customPlaceHolder.invokeCustomCallback(mainDocPart, temp);
	        	}
        	} catch (Exception e) {
        		System.out.println("An error occurred attempting to update the Custom place holders");
        	}
        }
	}
	
	/**
	 * Replace all table place holders
	 * @param mainDocPart MainDocumentPart o the template file
	 * @param placeHolders All table place holders
	 */
	private static void replaceTablePlaceHolders(MainDocumentPart mainDocPart, List<ITemplatePlaceHolder> placeHolders) {
		System.out.println("    replace the table place holders");
        
        Iterator<ITemplatePlaceHolder> iterator = placeHolders.iterator();
        while ( iterator.hasNext() ) {
        	ITemplatePlaceHolder placeHolder = iterator.next();
        	
        	// Prepare the XPath that will be applied to the template
        	String xpath = "//w:t[contains(text(),'%s')]";
        	xpath = String.format(xpath, placeHolder.getIdentifier());
        	
        	System.out.println("        XPath used " + xpath);
        	        	
        	TablePlaceHolder tablePlaceHolder = (TablePlaceHolder) placeHolder;
        	DocxUtils.populateTable(tablePlaceHolder, mainDocPart);
        }
	}
}

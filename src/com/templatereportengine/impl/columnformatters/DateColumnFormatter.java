package com.templatereportengine.impl.columnformatters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import com.templatereportengine.abstracts.AbstractTableColumnFormatter;
import com.templatereportengine.constants.placeholder.table.ColumnFormatterType;

public class DateColumnFormatter extends AbstractTableColumnFormatter {
	
	private String inputFormatString = "EEE MMM dd hh:mm:ss zzz yyyy";
	private String outputFormatString = "dd/mm/yyyy";
	
	/**
	 * Date formmater allows you to specify how you want a data value to be displayed.
	 * 
	 * See http://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html for date format strings
	 * 
	 * @param inputFormatString The format of the input date value
	 * @param outputFormatString The format of how the column value should appear
	 * 
	 */
	public DateColumnFormatter(String inputFormatString, String outputFormatString) {
		super(ColumnFormatterType.DATE);
		
		this.inputFormatString = inputFormatString;
		this.outputFormatString = outputFormatString;
	}
	
	public DateColumnFormatter(String outputFormatString) {
		super(ColumnFormatterType.DATE);
		
		this.outputFormatString = outputFormatString;
	}

	/*
	 * Format the column value as a date
	 */
	@Override
	public String format(String value) {
		if ( StringUtils.isNotBlank(value) ) {
			try {
				DateFormat dateFormat = new SimpleDateFormat(this.inputFormatString, Locale.ENGLISH);
				Date date = dateFormat.parse(value);
	
				dateFormat = new SimpleDateFormat(this.outputFormatString);
						
				value = dateFormat.format(date);
			} catch (Exception e) {
				System.out.println("DateColumnFormatter. Failed to format the date correctly");
			}
		}
		
		return value;
	}	
}

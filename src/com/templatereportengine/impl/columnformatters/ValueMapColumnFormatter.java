package com.templatereportengine.impl.columnformatters;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.templatereportengine.abstracts.AbstractTableColumnFormatter;
import com.templatereportengine.constants.placeholder.table.ColumnFormatterType;

public class ValueMapColumnFormatter extends AbstractTableColumnFormatter {
	
	Map<String, String> valueMap;
	
	/**
	 * Use the column value to reference the actual value in a Map.
	 * 
	 * When a reference value cannot be found <pre>VALUE_NOT_MAPPED</pre> will be displayed
	 * @param valueMap The map to use to reference column values with actual values
	 */
	public ValueMapColumnFormatter(Map<String, String> valueMap) {
		super(ColumnFormatterType.PREFIX);
		
		this.valueMap = valueMap;
	}

	/* 
	 * Format the value based on the value map
	 */
	@Override
	public String format(String value) {
		
		if ( StringUtils.isNotBlank(value) ) {
			value = this.valueMap.get(value);
			value = StringUtils.defaultIfEmpty(value, "VALUE NOT MAPPED");
		}
		
		return value;
	}	
}

package com.templatereportengine.impl.columnformatters;

import com.templatereportengine.abstracts.AbstractTableColumnFormatter;
import com.templatereportengine.constants.placeholder.table.ColumnFormatterType;

public class PrefixColumnFormatter extends AbstractTableColumnFormatter {
	
	String prefixValue;
	
	/**
	 * Prepend the column value with the specified value
	 * 
	 * @param prefixValue The value to prepend to the column
	 */
	public PrefixColumnFormatter(String prefixValue) {
		super(ColumnFormatterType.PREFIX);
		
		this.prefixValue = prefixValue;
	}

	/* 
	 * Prefix the column value
	*/
	@Override
	public String format(String value) {
		return this.prefixValue + value;
	}	
}

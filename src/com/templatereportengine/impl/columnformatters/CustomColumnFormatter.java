package com.templatereportengine.impl.columnformatters;

import com.templatereportengine.abstracts.AbstractTableColumnFormatter;
import com.templatereportengine.constants.placeholder.table.ColumnFormatterType;
import com.templatereportengine.interfaces.ICustomColumnFormatter;

public class CustomColumnFormatter extends AbstractTableColumnFormatter {
	
	private ICustomColumnFormatter customFormatter;
		
	/**
	 * Custom formatters invoke a callback method when a place holder name has been encountered
	 * @param customFormatter The callback method that will be invoked
	 */
	public CustomColumnFormatter(ICustomColumnFormatter customFormatter) {
		super(ColumnFormatterType.CUSTOM);
		
		this.customFormatter = customFormatter;
	}

	/**
	 * Format the date based on the implementation
	 * @param bean The entity that is currently being used to bind to the current table row
	 * @param value The value from bean that will be bound to the column
	 */
	public String format(Object bean, String value) {
		return customFormatter.format(bean, value);
	}

	/**
	 * Format the date based on the implementation
	 * @param value The value from bean that will be bound to the column
	 */
	@Override
	public final String format(String value) {
		return format(null, value);
	}
}


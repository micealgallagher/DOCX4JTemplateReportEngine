package com.templatereportengine.impl.columnformatters;

import com.templatereportengine.abstracts.AbstractTableColumnFormatter;
import com.templatereportengine.constants.placeholder.table.ColumnFormatterType;

public class PostfixColumnFormatter extends AbstractTableColumnFormatter {
	
	String postfixValue;
	
	/**
	 * Postfix column formatters specify a value to output after a column value
	 * @param postfixValue The value to specify after the column value
	 */
	public PostfixColumnFormatter(String postfixValue) {
		super(ColumnFormatterType.POSTFIX);
		
		this.postfixValue = postfixValue;
	}

	/* 
	 * Append the column value
	 */
	@Override
	public String format(String value) {
		return value + this.postfixValue;
	}	
}

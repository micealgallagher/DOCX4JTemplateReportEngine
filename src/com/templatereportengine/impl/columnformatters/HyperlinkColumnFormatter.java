package com.templatereportengine.impl.columnformatters;

import com.templatereportengine.abstracts.AbstractTableColumnFormatter;
import com.templatereportengine.constants.placeholder.table.ColumnFormatterType;

public class HyperlinkColumnFormatter extends AbstractTableColumnFormatter {
	
	/**
	 * Format the column value as a hyperlink
	 */
	public HyperlinkColumnFormatter() {
		super(ColumnFormatterType.HYPERLINK);
	}

	/*
	 * Format the value as a hyperlink
	 */
	@Override
	public String format(String value) {	
		return value;
	}	
}

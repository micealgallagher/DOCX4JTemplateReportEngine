package com.templatereportengine.impl.placeholders;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.templatereportengine.abstracts.AbstractPlaceHolder;
import com.templatereportengine.constants.placeholder.ValueType;

public class DatePlaceHolder extends AbstractPlaceHolder {

	public DatePlaceHolder(String identifier, Date value, String outputFormat) {
		super(identifier, ValueType.STRING, getDateValue(value, outputFormat));
	}
	
	private static String getDateValue(Date value, String outputFormat) {
		DateFormat dateFormat = new SimpleDateFormat();
		dateFormat = new SimpleDateFormat(outputFormat);
				
		return null == value ? "" : dateFormat.format(value);
	}
}

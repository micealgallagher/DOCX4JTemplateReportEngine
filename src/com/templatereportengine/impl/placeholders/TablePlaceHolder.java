package com.templatereportengine.impl.placeholders;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.templatereportengine.abstracts.AbstractPlaceHolder;
import com.templatereportengine.constants.placeholder.ValueType;
import com.templatereportengine.interfaces.ITableColumnFormatter;

public class TablePlaceHolder extends AbstractPlaceHolder {

	private List<?> dataSet;
	private Map<String, ITableColumnFormatter> columnFormatterMap;
	
	/**
	 * Binds a data set to a table in the template
	 * @param identifer - The identifier of the table in the template. Specified as <pre>{TABLE_NAME:<i>identifier</i>}</pre>
	 * @param dataSet - An array list of objects that should be bound to a table
	 */
	public TablePlaceHolder(String identifer, List<?> dataSet) {
		super(identifer, ValueType.TABLE);
		
		this.dataSet = dataSet;
	}
		
	/**
	 * Binds a data set to a table in the template with column formatting
	 * @param identifer - The identifier of the table in the template. Specified as <pre>{TABLE_NAME:<i>identifier</i>}</pre>
	 * @param dataSet - An array list of objects that should be bound to a table
	 * @param columnFormatterMap - List of column formatters that should be used to format column data
	 */
	public TablePlaceHolder(String identifer, List<?> dataSet, Map<String, ITableColumnFormatter> columnFormatterMap ) {
		super(identifer, ValueType.TABLE);
		
		this.dataSet = dataSet;
		this.columnFormatterMap = columnFormatterMap;
	}

	/**
	 * Return the data set that has been specified by the caller
	 * @return A java.util.List with no type
	 */
	public List<?> getDataSet() {
		return dataSet;
	}

	/**
	 * Set the dataset to be bound to the table
	 * @param dataSet java.util.LinkedList with no type to be bound
	 */
	public void setDataSet(LinkedList<?> dataSet) {
		this.dataSet = dataSet;
	}
	
	/**
	 * Column formatters specifed for the template table
	 * @return Map of each ITableColumnFormatter to format column data
	 */
	public Map<String, ITableColumnFormatter> getColumnFormatterMap() {
		return this.columnFormatterMap;
	}
}

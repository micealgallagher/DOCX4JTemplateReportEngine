package com.templatereportengine.impl.placeholders;

import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;

import com.templatereportengine.abstracts.AbstractPlaceHolder;
import com.templatereportengine.constants.placeholder.ValueType;
import com.templatereportengine.interfaces.ICustomPlaceHolderCallback;

public class CustomPlaceHolder extends AbstractPlaceHolder {
	
	ICustomPlaceHolderCallback customCallback;
	
	/**
	 * Specify a custom callback that allows you to directly interact with the XML of the document
	 * @param identifier - The identifier of the place holder
	 * @param customCallback - A callback method that will be invoked when the place holder is encountered
	 */
	public CustomPlaceHolder(String identifier, ICustomPlaceHolderCallback customCallback) {
		super(identifier, ValueType.CUSTOM, "", "");
		this.customCallback = customCallback;
	}
	
	/**
	 * Call the custom callback specified by the caller
	 * @param mainDocPart - The DOCX4J MainDocumentPart of the template
	 * @param obj - The DOCX4J object (typically org.docx4j.wml.Text object) that contains the place holder
	 */
	public void invokeCustomCallback(MainDocumentPart mainDocPart, Object obj) {
		customCallback.customCallback(mainDocPart, obj, this.getIdentifier());
	}
}

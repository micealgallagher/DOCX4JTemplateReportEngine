package com.templatereportengine.impl.placeholders;

import java.util.Map;

import com.templatereportengine.abstracts.AbstractPlaceHolder;
import com.templatereportengine.constants.placeholder.ValueType;

public class ValueMapPlaceHolder extends AbstractPlaceHolder {

	private Map<String, String> valueMap;
	
	/**
	 * Format place holder using a map. Place holder values will be used to reference the value to be used in the map. When no mapped value is decteded VALUE_NOT_MAPPED will be dispalyed
	 * @param identifier - The identifier of the place holder that should be replaced with the map value
	 * @param value - The value to be used as a key in the valueMap for the value to be used
	 * @param valueMap - The valueMap which contains reference values
	 */
	public ValueMapPlaceHolder(String identifier, String value, Map<String, String> valueMap) {
		super(identifier, ValueType.VALUE_MAP, value);
		
		this.valueMap = valueMap;
	}
	
	/**
	 * Get the value map to be used
	 * @return a Map<String, String> that contains the lookup values
	 */
	public Map<String, String> getValueMap() {
		return valueMap;
	}

}
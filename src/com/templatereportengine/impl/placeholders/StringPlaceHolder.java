package com.templatereportengine.impl.placeholders;

import com.templatereportengine.abstracts.AbstractPlaceHolder;
import com.templatereportengine.constants.placeholder.ValueType;

public class StringPlaceHolder extends AbstractPlaceHolder {

	/**
	 * Format place holder value as a string
	 * 
	 * @param identifier The identifier of the place holder in the template
	 * @param value The value to replace the place holder with
	 */
	public StringPlaceHolder(String identifier, String value) {
		super(identifier, ValueType.STRING, value);
	}
}

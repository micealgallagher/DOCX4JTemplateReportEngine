package com.templatereportengine.impl.placeholders;

import com.templatereportengine.abstracts.AbstractPlaceHolder;
import com.templatereportengine.constants.placeholder.ValueType;

public class HyperlinkPlaceHolder extends AbstractPlaceHolder {
	
	
	/**
	 * Convert place holder value to a hyperlink. Make sure the place holder is a hyperlink in your template
	 * 
	 * @param identifier  The identifier of your place holder in the template
	 * @param text The text of the hyperlink 
	 * @param url The url (target) of the hyperlink
	 */
	public HyperlinkPlaceHolder(String identifier, String text, String url) {
		super(identifier, ValueType.HYPERLINK, url, text);
	}
}

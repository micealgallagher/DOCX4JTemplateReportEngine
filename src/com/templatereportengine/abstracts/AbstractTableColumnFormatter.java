package com.templatereportengine.abstracts;

import com.templatereportengine.constants.placeholder.table.ColumnFormatterType;
import com.templatereportengine.interfaces.ITableColumnFormatter;

public abstract class AbstractTableColumnFormatter implements ITableColumnFormatter {
	private ColumnFormatterType columnFormatter;
	
	/**
	 * Create an abstract table column formatter
	 * @param columnFormatterType The type of table column formatter
	 */
	public AbstractTableColumnFormatter(ColumnFormatterType columnFormatterType) {
		this.columnFormatter = columnFormatterType;
	}
	
	@Override
	public ColumnFormatterType getColumnFormatter() {
		return this.columnFormatter;
	}
	
	@Override
	public String format(Object bean, String value) {
		return "";
	}
}

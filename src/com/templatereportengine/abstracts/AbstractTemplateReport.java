package com.templatereportengine.abstracts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.templatereportengine.constants.placeholder.ValueType;
import com.templatereportengine.interfaces.ITemplatePlaceHolder;
import com.templatereportengine.interfaces.ITemplateReport;

public abstract class AbstractTemplateReport implements ITemplateReport {

	public String templateLocation;
	public final List<ITemplatePlaceHolder> placeHolders;
	
	/**
	 * An abstract representation of a report
	 * @param templateLocation The location of the report template
	 * @param placeHolders A list of template place holders
	 */
	public AbstractTemplateReport(String templateLocation, List<ITemplatePlaceHolder> placeHolders) {
		this.templateLocation = templateLocation;
		this.placeHolders = placeHolders;
	}
	
	@Override
	public final String getTemplateLocation() {
		return this.templateLocation;
	}
	
	@Override
	public final List<ITemplatePlaceHolder> getPlaceHolders() {
		return this.placeHolders;
	}
	
	@Override
	public final Iterator<ITemplatePlaceHolder> getPlaceHolderIterator() {
		return this.placeHolders.iterator();
	}
	
	@Override
	public final List<ITemplatePlaceHolder> getPlaceHoldersByType(ValueType type) {
		List<ITemplatePlaceHolder> placeHolders = new ArrayList<ITemplatePlaceHolder>();
		
		Iterator<ITemplatePlaceHolder> iterator = getPlaceHolderIterator();
		while ( iterator.hasNext() ) {
			ITemplatePlaceHolder placeHolder = iterator.next();
			
			if ( placeHolder.getType() == type ) {
				placeHolders.add(placeHolder);
			}
		}
		
		return placeHolders;
	}
}

package com.templatereportengine.abstracts;

import com.templatereportengine.constants.placeholder.ValueType;
import com.templatereportengine.interfaces.ITemplatePlaceHolder;

public abstract class AbstractPlaceHolder implements ITemplatePlaceHolder {

	private ValueType type; 
	private String identifier;
	private String value;
	private String text;
	
	/**
	 * An abstract place holder
	 * @param identifier The identifier of the place holder
	 * @param type The type of place holder
	 * @param value The value the place holder will be replaced with
	 */
	public AbstractPlaceHolder(String identifier, ValueType type, String value) {
		this.identifier = identifier;
		this.type = type;
		this.value = value;
	}
	
	/**
	 * An abstract place holder
	 * @param identifier The identifier of the place holder
	 * @param type The type of the place holder
	 * @param value The value of the place holder text
	 * @param text The text that the place holder will be replaced with
	 */
	public AbstractPlaceHolder(String identifier, ValueType type, String value, String text) {
		this.identifier = identifier;
		this.type = type;
		this.value = value;
		this.text = text;
	}
	
	/**
	 * An abstract place holder
	 * @param identifier The identifier of the place holder
	 * @param type The type of the place holder
	 */
	public AbstractPlaceHolder(String identifier, ValueType type) {
		this.identifier = identifier;
		this.type = type;
		this.value = "";
		this.text = "";
	}
	
	@Override
	public String getValue() {
		return this.value;
	}

	@Override
	public final ValueType getType() {
		return this.type;
	}
	
	@Override
	public final String getIdentifier() {
		return this.identifier;
	}

	@Override
	public String getText() {
		return this.text;
	}

}

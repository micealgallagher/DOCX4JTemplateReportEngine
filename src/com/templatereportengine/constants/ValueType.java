package com.templatereportengine.constants;

public enum ValueType {
	STRING,
	HYPERLINK,
	TABLE,
	CUSTOM,
	VALUE_MAP
}
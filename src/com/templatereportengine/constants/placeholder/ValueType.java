package com.templatereportengine.constants.placeholder;

public enum ValueType {
	STRING,
	HYPERLINK,
	TABLE,
	CUSTOM,
	VALUE_MAP,
	DATE
}
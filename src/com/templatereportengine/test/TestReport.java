package com.templatereportengine.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.Text;

import com.templatereportengine.abstracts.AbstractTemplateReport;
import com.templatereportengine.impl.columnformatters.CustomColumnFormatter;
import com.templatereportengine.impl.columnformatters.DateColumnFormatter;
import com.templatereportengine.impl.columnformatters.HyperlinkColumnFormatter;
import com.templatereportengine.impl.columnformatters.PostfixColumnFormatter;
import com.templatereportengine.impl.columnformatters.ValueMapColumnFormatter;
import com.templatereportengine.impl.placeholders.CustomPlaceHolder;
import com.templatereportengine.impl.placeholders.HyperlinkPlaceHolder;
import com.templatereportengine.impl.placeholders.StringPlaceHolder;
import com.templatereportengine.impl.placeholders.TablePlaceHolder;
import com.templatereportengine.impl.placeholders.ValueMapPlaceHolder;
import com.templatereportengine.interfaces.ICustomColumnFormatter;
import com.templatereportengine.interfaces.ICustomPlaceHolderCallback;
import com.templatereportengine.interfaces.ITableColumnFormatter;
import com.templatereportengine.interfaces.ITemplatePlaceHolder;

public class TestReport extends AbstractTemplateReport {
	
	private final static String templateLocation = "./src/com/templatereportengine/resources/template.docx";
	
	public TestReport(List<TestPerson> people) {
		super(templateLocation, initalizePlaceHolders(people));
	}
	
	private static List<ITemplatePlaceHolder> initalizePlaceHolders(List<TestPerson> people) {
		List<ITemplatePlaceHolder> placeHolders = new ArrayList<ITemplatePlaceHolder>();
		Map<String, String> statusValueMap = new HashMap<String, String>();
		Map<String, String> nationalityValueMap = new HashMap<String, String>();
		Map<String, ITableColumnFormatter> columnFormatterMap = new HashMap<String, ITableColumnFormatter>(); 

		// Prepare the nationality map
		nationalityValueMap.put("CAN", "Canadian");
		nationalityValueMap.put("IRL", "Irish");
		nationalityValueMap.put("FRN", "French");
		nationalityValueMap.put("GB", "English");
		
		// Prepare the status map
		statusValueMap.put("L", "Low Priority");
		statusValueMap.put("M", "Medium Priority");
		statusValueMap.put("H", "High Priority");
		
		
		columnFormatterMap.put("forename", new CustomColumnFormatter(customColumnFormatter));
		columnFormatterMap.put("age", new PostfixColumnFormatter(" <-- too old!"));
		columnFormatterMap.put("nationality", new ValueMapColumnFormatter(nationalityValueMap));
		columnFormatterMap.put("dob", new DateColumnFormatter("EEE-mm-yyyy"));
		columnFormatterMap.put("website", new HyperlinkColumnFormatter());
		
		// Prepare the place holders
		placeHolders.add(new StringPlaceHolder("%%NOW%%", Calendar.getInstance().getTime().toString()));
		placeHolders.add(new HyperlinkPlaceHolder("%%WEBSITE%%", "Miceal Gallagher", "http://mehaul.me"));
		placeHolders.add(new ValueMapPlaceHolder("%%STATUS%%", "M", statusValueMap));
		placeHolders.add(new CustomPlaceHolder("%%CUSTOM%%", formatCustomPlaceHolder));
		placeHolders.add(new TablePlaceHolder("people", people, columnFormatterMap));
		
		return placeHolders;
	}
	
	public static ICustomPlaceHolderCallback formatCustomPlaceHolder = new ICustomPlaceHolderCallback() {
		
		@Override
		public void customCallback(MainDocumentPart mainDocPart, Object obj, String identifier) {
			Text text = (Text) obj;

    		String orginialText = text.getValue();
    		String modifiedText = orginialText.replace(identifier, "Custom callback created content");
    		
    		text.setValue(modifiedText);
		}
	};
	
	public static ICustomColumnFormatter customColumnFormatter = new ICustomColumnFormatter() {
		
		@Override
		public String format(Object bean, String value) {
			
			if ( value.contains("Miceal") ) {
				value = value + " THIS IS ME YO!";
			}
			
			return value;
		}
	};
}

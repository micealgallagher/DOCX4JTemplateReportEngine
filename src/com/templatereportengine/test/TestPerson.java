package com.templatereportengine.test;

import java.util.Date;

public class TestPerson {
	private String forename;
	private String surname;
	private String nationality;
	private int age;
	private Date dob;
	private String website;
	
	public TestPerson( String forename, String surname, int age, String nationality, Date dob, String website) {
		this.forename = forename;
		this.surname = surname;
		this.age = age;
		this.nationality = nationality;
		this.setDob(dob);
		this.website = website;
	}

	public String getForename() {
		return forename;
	}

	public void setForename(String forename) {
		this.forename = forename;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}	
}

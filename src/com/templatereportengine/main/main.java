package com.templatereportengine.main;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.templatereportengine.engine.TemplateReportGenerator;
import com.templatereportengine.test.PersonReport;
import com.templatereportengine.test.TestPerson;
import com.templatereportengine.test.TestReport;

public class main {

	public main() {}
	
	public static void main(String[] args) {
		System.out.println("About to create summary");

		List<TestPerson> people = new ArrayList<TestPerson>(); 
		people.add(new TestPerson("Miceal", "Gallagher", 28, "IRL", Calendar.getInstance().getTime(), "http://mehaul.me"));
		people.add(new TestPerson("Emily", "Pearlman", 33, "CAN", Calendar.getInstance().getTime(), "http://emily.me"));
		people.add(new TestPerson("Jane", "Ellens", 61, "GB", Calendar.getInstance().getTime(), "http://jane.me"));
		people.add(new TestPerson("Katrina", "O'Reilly", 30, "IRL", Calendar.getInstance().getTime(), "http://katrina.me"));
		people.add(new TestPerson("Pinewood", "Pete", 30, "FRN", Calendar.getInstance().getTime(), "http://pinewood.me"));
		
		try {
			File output = new File("./src/com/templatereportengine/resources/output.docx");
			TestReport testReport = new TestReport(null);
			
			TemplateReportGenerator.generateReport(testReport, output);
			
		} catch (Exception e) {
			System.out.println("Looks like something went wrong I'm afraid.");
			e.printStackTrace();
		}
		
		System.out.println("Finished creating summary");
	}

}

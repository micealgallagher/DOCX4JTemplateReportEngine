package com.templatereportengine.interfaces;

import com.templatereportengine.constants.placeholder.ValueType;

public interface ITemplatePlaceHolder {
	public ValueType getType();
	public String getText();
	public String getIdentifier();
	String getValue();
}

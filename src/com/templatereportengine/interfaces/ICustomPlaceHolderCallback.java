package com.templatereportengine.interfaces;

import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;

public interface ICustomPlaceHolderCallback {
	public void customCallback(MainDocumentPart mainDocPart, Object obj, String identifier);
}

package com.templatereportengine.interfaces;

import com.templatereportengine.constants.placeholder.table.ColumnFormatterType;

public interface ITableColumnFormatter {
	public ColumnFormatterType getColumnFormatter();
	public String format(String value);
	public abstract String format(Object bean, String value);
}

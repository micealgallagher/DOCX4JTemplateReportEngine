package com.templatereportengine.interfaces;

public interface ICustomColumnFormatter {
	public String format(Object bean, String value);
}

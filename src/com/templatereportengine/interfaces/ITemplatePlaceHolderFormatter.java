package com.templatereportengine.interfaces;

import com.templatereportengine.constants.placeholder.ValueType;

public interface ITemplatePlaceHolderFormatter {
	public String formatPlaceHolder(Object bean, ValueType type, Object value);
}

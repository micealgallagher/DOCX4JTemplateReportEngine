package com.templatereportengine.interfaces;

import java.util.Iterator;
import java.util.List;

import com.templatereportengine.constants.placeholder.ValueType;

public interface ITemplateReport {
	public String templateLocation = "";
	public String getTemplateLocation();
	public List<ITemplatePlaceHolder> getPlaceHolders();
	public Iterator<ITemplatePlaceHolder> getPlaceHolderIterator();
	List<ITemplatePlaceHolder> getPlaceHoldersByType(ValueType type);
}
